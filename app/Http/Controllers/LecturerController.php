<?php

namespace App\Http\Controllers;

use App\academic;
use App\accounts;
use App\admission;
use App\computerlab;
use App\dean;
use App\department;
use App\electroniclab;
use App\ess;
use App\examunit;
use App\finance;
use App\hod;
use App\library;
use App\registry;
use App\society;
use App\student;
use App\studentaffairs;
use App\User;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;


class LecturerController extends Controller
{
    public function __construct(){
	    $this->middleware('auth');
    }

	public function index() {

		if(Auth::user()->role == "ADMIN"){
			return redirect('/admin');
		}

		$int  = Input::get("page");
		if(is_null($int) || empty($int)) $int = 1;

		if(Auth::user()->role == "EXAM UNIT"){
			$examsArray = array();
			$exam = student::all();

			$allStudents = student::all()->forPage($int,10);

			foreach($exam as $item){
				array_push($examsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $examsArray)->forPage($int,10);
			$total = count(student::all());
			$maxPage = count(student::all()) / 10;

		}

		if(Auth::user()->role == "LIBRARY"){

			$allStudents = student::all()->forPage($int,10);


			$libArray = array();
			$library = library::all();

			foreach($library as $item){
				array_push($libArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $libArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $libArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ADMISSION"){
			$allStudents = student::all()
				->where("isAcademicCleared",1)
				->forPage($int,10);
			$admissionArray = array();
			$admission = admission::all();
			foreach($admission as $item){
				array_push($admissionArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $admissionArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $admissionArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACCOUNTS"){

			$allStudents = student::all()->forPage($int,10);
			$accountsArray = array();
			$accounts = accounts::all();
			foreach($accounts as $item){
				array_push($accountsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $accountsArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $accountsArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACADEMIC"){
			$academicArray = array();

			$academicApproved = array();

			$academic = academic::all();

			foreach($academic as $item){
				array_push($academicApproved,$item->studentid);
			}

			$library = library::all();
			$society = society::all();
			$computerLab = computerlab::all();
			$electronicLab = electroniclab::all();
			$accounts = accounts::all();

			foreach($library as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($society as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($computerLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($electronicLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($accounts as $item){
				array_push($academicArray,$item->studentid);
			}

			$allStudents = student::all()->whereIn("studentid", $academicArray)->forPage($int,10);
			$students = student::all()->whereIn("studentid", $academicApproved)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $academicApproved));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "HOD"){
			$hodArray = array();
			$hod = hod::all();
			foreach($hod as $item){
				array_push($hodArray,$item->studentid);
			}

			$allStudents = student::all()
				->where("isEssCleared",1)
				->forPage($int,10);

			$students = student::all()->whereIn("studentid", $hodArray)
				->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $hodArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "ESS"){
			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$allStudents = student::all()
				->where("isAdmissionCleared",1)
                ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "DEAN"){
			$deanArray = array();
			$dean = dean::all();
			foreach($dean as $item){
				array_push($deanArray,$item->studentid);
			}

			$allStudents = student::all()
				->where("isHodCleared",1)
                ->forPage($int,10);


			$students = student::all()->whereIn("studentid", $deanArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $deanArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "SOCIETY"){

			$allStudents = student::all()->forPage($int,10);

			$societyArray = array();
			$society = society::all();
			foreach($society as $item){
				array_push($societyArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $societyArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $societyArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "COMPUTER LAB"){

			$allStudents = student::all()->forPage($int,10);

			$computerLabArray = array();
			$computerLab = computerlab::all();
			foreach($computerLab as $item){
				array_push($computerLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $computerLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $computerLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ELECTRONIC LAB"){
			$allStudents = student::all()->forPage($int,10);
			$electronicLabArray = array();
			$electronicLab = electroniclab::all();
			foreach($electronicLab as $item){
				array_push($electronicLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $electronicLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $electronicLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ESS"){
			$allStudents = student::all()
				->where("isAdmissionCleared",1)
				->forPage($int,10);

			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}


		return view('staff',
			[
				'students' => $students,
				'page' => $int,
				'max' => count(student::all()) - $total /10,
				'total' => $total,
				'totalProspective' => count($allStudents),
				'allStudents' => $allStudents
			]);
	}

	public function getStaff() {
		$staff = User::all();
		return view('manageStaff',[
			'staff' => $staff
		]);
	}

	public function deleteUser($lid ,Request $request ) {
		 User::destroy($lid);
		$request->session()->flash("success","Staff Deleted");

		return redirect('/admin');

	}

	public function unclearStudent(Request $request, $sid ) {


			if ( Auth::user()->role == "SOCIETY" ) {
				society::where('studentid',$sid)->delete();

			}
			else if ( Auth::user()->role == "COMPUTER LAB" ) {

				computerlab::where('studentid',$sid)->delete();
			}
			else if ( Auth::user()->role == "ELECTRONIC LAB" ) {
				electroniclab::where('studentid',$sid)->delete();

			}
			else if ( Auth::user()->role == "ACADEMIC" ) {
				academic::where('studentid',$sid)->delete();

			}
			else if ( Auth::user()->role == "ADMISSION" ) {
				admission::where('studentid',$sid)->delete();
			}
			else if ( Auth::user()->role == "HOD" ) {
				hod::where('studentid',$sid)->delete();
			}
			else if ( Auth::user()->role == "ACCOUNTS" ) {
				accounts::where('studentid',$sid)->delete();
			}
			else if ( Auth::user()->role == "LIBRARY" ) {
				library::where('studentid',$sid)->delete();


			}
			else if ( Auth::user()->role == "ESS" ) {
				ess::where('studentid',$sid)->delete();
			} else if ( Auth::user()->role == "DEAN" ) {

				dean::where('studentid',$sid)->delete();

			} else {
				return redirect( "/staff" );
			}



		$request->session()->flash("success","Student Un-cleared");

		return redirect('/staff');

	}

	public function viewTranscript() {


		if(Auth::user()->role == "ADMIN"){
			return redirect('/admin');
		}

		$int  = Input::get("page");
		if(is_null($int) || empty($int)) $int = 1;

		if(Auth::user()->role == "EXAM UNIT"){
			$examsArray = array();
			$exam = student::all();

			$allStudents = student::all()->forPage($int,10);

			foreach($exam as $item){
				array_push($examsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $examsArray)->forPage($int,10);
//			$students = $allStudents;
			$total = count(student::all());
			$maxPage = count(student::all()) / 10;

		}

		if(Auth::user()->role == "LIBRARY"){

			$allStudents = student::all()->forPage($int,10);


			$libArray = array();
			$library = library::all();

			foreach($library as $item){
				array_push($libArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $libArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $libArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ADMISSION"){
			$allStudents = student::all()
			                      ->where("isAcademicCleared",1)
			                      ->forPage($int,10);
			$admissionArray = array();
			$admission = admission::all();
			foreach($admission as $item){
				array_push($admissionArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $admissionArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $admissionArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACCOUNTS"){

			$allStudents = student::all()->forPage($int,10);
			$accountsArray = array();
			$accounts = accounts::all();
			foreach($accounts as $item){
				array_push($accountsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $accountsArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $accountsArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACADEMIC"){
			$academicArray = array();

			$academicApproved = array();

			$academic = academic::all();

			foreach($academic as $item){
				array_push($academicApproved,$item->studentid);
			}

			$library = library::all();
			$society = society::all();
			$computerLab = computerlab::all();
			$electronicLab = electroniclab::all();
			$accounts = accounts::all();

			foreach($library as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($society as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($computerLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($electronicLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($accounts as $item){
				array_push($academicArray,$item->studentid);
			}

			$allStudents = student::all()->whereIn("studentid", $academicArray)->forPage($int,10);
			$students = student::all()->whereIn("studentid", $academicApproved)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $academicApproved));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "HOD"){
			$hodArray = array();
			$hod = hod::all();
			foreach($hod as $item){
				array_push($hodArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isEssCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $hodArray)
			                   ->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $hodArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "ESS"){
			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "DEAN"){
			$deanArray = array();
			$dean = dean::all();
			foreach($dean as $item){
				array_push($deanArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isHodCleared",1)
			                      ->forPage($int,10);


			$students = student::all()->whereIn("studentid", $deanArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $deanArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "SOCIETY"){

			$allStudents = student::all()->forPage($int,10);

			$societyArray = array();
			$society = society::all();
			foreach($society as $item){
				array_push($societyArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $societyArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $societyArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "COMPUTER LAB"){

			$allStudents = student::all()->forPage($int,10);

			$computerLabArray = array();
			$computerLab = computerlab::all();
			foreach($computerLab as $item){
				array_push($computerLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $computerLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $computerLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ELECTRONIC LAB"){
			$allStudents = student::all()->forPage($int,10);
			$electronicLabArray = array();
			$electronicLab = electroniclab::all();
			foreach($electronicLab as $item){
				array_push($electronicLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $electronicLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $electronicLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ESS"){
			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}


		return view('viewTranscript',
			[
				'students' => $students,
				'page' => $int,
				'max' => count(student::all()) /10,
				'total' => $total,
				'totalProspective' => count($allStudents),
				'allStudents' => $allStudents
			]);
	}

	public function viewCleared(){

		if(Auth::user()->role == "ADMIN"){
			return redirect('/admin');
		}

		$int  = Input::get("page");
		if(is_null($int) || empty($int)) $int = 1;

		if(Auth::user()->role == "EXAM UNIT"){
			$examsArray = array();
			$exam = student::all();

			$allStudents = student::all()->forPage($int,10);

			foreach($exam as $item){
				array_push($examsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $examsArray)->forPage($int,10);
			$total = count(student::all());
			$maxPage = count(student::all()) / 10;

		}

		if(Auth::user()->role == "LIBRARY"){

			$allStudents = student::all()->forPage($int,10);


			$libArray = array();
			$library = library::all();

			foreach($library as $item){
				array_push($libArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $libArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $libArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ADMISSION"){
			$allStudents = student::all()
			                      ->where("isAcademicCleared",1)
			                      ->forPage($int,10);
			$admissionArray = array();
			$admission = admission::all();
			foreach($admission as $item){
				array_push($admissionArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $admissionArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $admissionArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACCOUNTS"){

			$allStudents = student::all()->forPage($int,10);
			$accountsArray = array();
			$accounts = accounts::all();
			foreach($accounts as $item){
				array_push($accountsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $accountsArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $accountsArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ACADEMIC"){
			$academicArray = array();

			$academicApproved = array();

			$academic = academic::all();

			foreach($academic as $item){
				array_push($academicApproved,$item->studentid);
			}

			$library = library::all();
			$society = society::all();
			$computerLab = computerlab::all();
			$electronicLab = electroniclab::all();
			$accounts = accounts::all();

			foreach($library as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($society as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($computerLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($electronicLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($accounts as $item){
				array_push($academicArray,$item->studentid);
			}

			$allStudents = student::all()->whereIn("studentid", $academicArray)->forPage($int,10);
			$students = student::all()->whereIn("studentid", $academicApproved)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $academicApproved));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "HOD"){
			$hodArray = array();
			$hod = hod::all();
			foreach($hod as $item){
				array_push($hodArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isEssCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $hodArray)
			                   ->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $hodArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "ESS"){
			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "DEAN"){
			$deanArray = array();
			$dean = dean::all();
			foreach($dean as $item){
				array_push($deanArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isHodCleared",1)
			                      ->forPage($int,10);


			$students = student::all()->whereIn("studentid", $deanArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $deanArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "SOCIETY"){

			$allStudents = student::all()->forPage($int,10);

			$societyArray = array();
			$society = society::all();
			foreach($society as $item){
				array_push($societyArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $societyArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $societyArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "COMPUTER LAB"){

			$allStudents = student::all()->forPage($int,10);

			$computerLabArray = array();
			$computerLab = computerlab::all();
			foreach($computerLab as $item){
				array_push($computerLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $computerLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $computerLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ELECTRONIC LAB"){
			$allStudents = student::all()->forPage($int,10);
			$electronicLabArray = array();
			$electronicLab = electroniclab::all();
			foreach($electronicLab as $item){
				array_push($electronicLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $electronicLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $electronicLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ESS"){
			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}


		return view('viewCleared',
			[
				'students' => $students,
				'page' => $int,
				'max' =>  $total /10,
				'total' => $total,
				'totalProspective' => count($allStudents),
				'allStudents' => $allStudents
			]);
	}

	public function getClearStudents() {

		if(Auth::user()->role == "ADMIN"){
			return redirect('/admin');
		}

		$int  = Input::get("page");
		if(is_null($int) || empty($int)) $int = 1;

		if(Auth::user()->role == "EXAM UNIT"){
			$examsArray = array();
			$exam = student::all();

			$allStudents = student::all()->forPage($int,10);

			foreach($exam as $item){
				array_push($examsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $examsArray)->forPage($int,10);
			$total = count(student::all());
			$maxPage = count(student::all()) / 10;

		}

		if(Auth::user()->role == "LIBRARY"){

			$allStudents = student::all()->forPage($int,10);


			$libArray = array();
			$library = library::all();

			foreach($library as $item){
				array_push($libArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $libArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $libArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ADMISSION"){
			$allStudents = student::all()
			                      ->where("isAcademicCleared",1)
			                      ->forPage($int,10);
			$admissionArray = array();
			$admission = admission::all();
			foreach($admission as $item){
				array_push($admissionArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $admissionArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $admissionArray));
			$maxPage = count($students) / 10;

		}


		if(Auth::user()->role == "ACADEMIC"){
			$academicArray = array();

			$academicApproved = array();

			$academic = academic::all();

			foreach($academic as $item){
				array_push($academicApproved,$item->studentid);
			}

			$library = library::all();
			$society = society::all();
			$computerLab = computerlab::all();
			$electronicLab = electroniclab::all();
			$accounts = accounts::all();

			foreach($library as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($society as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($computerLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($electronicLab as $item){
				array_push($academicArray,$item->studentid);
			}

			foreach($accounts as $item){
				array_push($academicArray,$item->studentid);
			}

			$allStudents = student::all()->whereIn("studentid", $academicArray)->forPage($int,10);
			$students = student::all()->whereIn("studentid", $academicApproved)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $academicApproved));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "HOD"){
			$hodArray = array();
			$hod = hod::all();
			foreach($hod as $item){
				array_push($hodArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isEssCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $hodArray)
			                   ->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $hodArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "ESS"){
			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "DEAN"){
			$deanArray = array();
			$dean = dean::all();
			foreach($dean as $item){
				array_push($deanArray,$item->studentid);
			}

			$allStudents = student::all()
			                      ->where("isHodCleared",1)
			                      ->forPage($int,10);


			$students = student::all()->whereIn("studentid", $deanArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $deanArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "SOCIETY"){

			$allStudents = student::all()->where('society',Auth::user()->societyName)->forPage($int,10);

			$societyArray = array();
			$society = society::all();
			foreach($society as $item){
				array_push($societyArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $societyArray)->forPage($int,10);
			$total = count(student::all()->where('society',Auth::user()->societyName)->whereIn("studentid", $societyArray));
			$maxPage = count($students) / 10;

		}


		if(Auth::user()->role == "COMPUTER LAB"){

			$allStudents = student::all()->forPage($int,10);

			$computerLabArray = array();
			$computerLab = computerlab::all();
			foreach($computerLab as $item){
				array_push($computerLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $computerLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $computerLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ELECTRONIC LAB"){
			$allStudents = student::all()->forPage($int,10);
			$electronicLabArray = array();
			$electronicLab = electroniclab::all();
			foreach($electronicLab as $item){
				array_push($electronicLabArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $electronicLabArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $electronicLabArray));
			$maxPage = count($students) / 10;

		}

		if(Auth::user()->role == "ESS"){
			$allStudents = student::all()
			                      ->where("isAdmissionCleared",1)
			                      ->forPage($int,10);

			$essArray = array();
			$ess = ess::all();
			foreach($ess as $item){
				array_push($essArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $essArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $essArray));
			$maxPage = count($students) / 10;


		}

		if(Auth::user()->role == "ACCOUNTS"){

			$allStudents = student::all()->forPage($int,10);
			$accountsArray = array();
			$accounts = accounts::all();
			foreach($accounts as $item){
				array_push($accountsArray,$item->studentid);
			}

			$students = student::all()->whereIn("studentid", $accountsArray)->forPage($int,10);
			$total = count(student::all()->whereIn("studentid", $accountsArray));
			$maxPage = count($students) / 10;

		}



		return view('clearStudents',
			[
				'students' => $students,
				'page' => $int,
				'max' => count(student::all()) /10,
				'total' => $total,
				'totalProspective' => count($allStudents),
				'allStudents' => $allStudents
			]);

	}

	public function getUploadStudents() {
		$int  = Input::get("page");
		if(is_null($int) || empty($int)) $int = 1;

		if(Auth::user()->role == "EXAM UNIT"){

			$students = student::all()->forPage($int,10);
			$total = count(student::all());
			$maxPage = count(student::all()) / 10;

		}


		return view('uploadStudents',
			[
				'students' => $students,
				'page' => $int,
				'max' => $maxPage,
				'total' => $total
			]);

	}

	public function postClearStudent(Request $request){

		try {
			if ( Auth::user()->role == "SOCIETY" ) {
				$society            = new society();
				$society->studentid = $request->input( 'studentid' );
				$status             = $society->save();
			} else if ( Auth::user()->role == "COMPUTER LAB" ) {
				$computerLab            = new computerlab();
				$computerLab->studentid = $request->input( 'studentid' );
				$status                 = $computerLab->save();

			} else if ( Auth::user()->role == "ELECTRONIC LAB" ) {
				$electronicLab            = new electroniclab();
				$electronicLab->studentid = $request->input( 'studentid' );
				$status                   = $electronicLab->save();

			} else if ( Auth::user()->role == "ACADEMIC" ) {
				$academic            = new academic();
				$academic->studentid = $request->input( 'studentid' );
				$status              = $academic->save();

				$student                    = student::where( 'studentid', $request->input( 'studentid' ) )->first();
				$student->isAcademicCleared = 1;
				$student->save();

			} else if ( Auth::user()->role == "ADMISSION" ) {
				$admision            = new admission();
				$admision->studentid = $request->input( 'studentid' );
				$status              = $admision->save();

				$student                     = student::where( 'studentid', $request->input( 'studentid' ) )->first();
				$student->isAdmissionCleared = 1;
				$student->save();

			} else if ( Auth::user()->role == "HOD" ) {
				$hod            = new hod();
				$hod->studentid = $request->input( 'studentid' );
				$status         = $hod->save();

				$student               = student::where( 'studentid', $request->input( 'studentid' ) )->first();
				$student->isHodCleared = 1;
				$student->save();

			} else if ( Auth::user()->role == "ACCOUNTS" ) {
				$accounts            = new accounts();
				$accounts->studentid = $request->input( 'studentid' );
				$status              = $accounts->save();
			} else if ( Auth::user()->role == "LIBRARY" ) {
				$library            = new library();
				$library->studentid = $request->input( 'studentid' );
				$status             = $library->save();

			} else if ( Auth::user()->role == "ESS" ) {
				$ess            = new ess();
				$ess->studentid = $request->input( 'studentid' );
				$status         = $ess->save();

				$student               = student::where( 'studentid', $request->input( 'studentid' ) )->first();
				$student->isEssCleared = 1;
				$student->save();

			} else if ( Auth::user()->role == "DEAN" ) {
				$dean            = new dean();
				$dean->studentid = $request->input( 'studentid' );
				$status          = $dean->save();

			} else {
				return redirect( "/staff" );
			}


			$request->session()->flash( 'success', 'Students Cleared' );

			return redirect( '/staff/clear-students' );

		} catch(Exception $e){
			$request->session()->flash("error","Sorry, you are not authorized to clear that student.");
			return redirect('/staff/clear-students');
		}
	}

	public function postBulkClearStudent( Request $request ) {

		try {


			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/students/",$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/students/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/students/".$inputFileName);

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);


			$count=1;

			$role = Auth::user()->role;

			// add results to data base from file
			foreach($rowData as $cell){



				if($role == "LIBRARY" ){

					$student = new library();
					$student->studentid = $cell[0];
					$status = $student->save();
				}

				if($role == "FINANCE" ){

					$student = new finance();
					$student->studentid = $cell[0];
					$status = $student->save();
				}
				if($role == "DEPARTMENT" ){

					$student = new department();
					$student->studentid = $cell[0];
					$status = $student->save();
				}
				if($role == "ACADEMIC" ){

					$student = new academic();
					$student->studentid = $cell[0];
					$status = $student->save();
				}
				if($role == "STUDENT AFFAIRS" ){

					$student = new studentaffairs();
					$student->studentid = $cell[0];
					$status = $student->save();
				}
				if($role == "ESS" ){

					$student = new ess();
					$student->studentid = $cell[0];
					$status = $student->save();
				}
				if($role == "REGISTRY" ){

					$student = new registry();
					$student->studentid = $cell[0];
					$status = $student->save();
				}

			}


		}
		catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


		$request->session()->flash('status', 'Students Cleared');
		return redirect('/staff');

	}

	public function postAddStudents(Request $request) {

		$password = Str::random(6);

		$student = new student();
		$student->studentid = $request->input('studentid');
		$student->surname = $request->input('surname');
		$student->othernames = $request->input('othernames');
		$student->society = $request->input('society');
		$student->email = $request->input('email');
		$student->gender = $request->input('gender');
		$student->nationality = $request->input('nationality');
		$student->level = $request->input('level');
		$student->session = $request->input('session');
		$student->prog = $request->input('programme');
		$student->password = $password;
		$status =	$student->save();



		mail($request->input('email'),"Your clearance portal credentials", "You have successfully been added as a potential
		graduand. Please visit the clearance portal at ". url('/') . ". Login with your ID number and this password: $password" );



		if($status) $report = "Student added successfully!"; else $report = "Sorry an error occured";

		$students = student::all();
		return view('staff', [ 'status' => $report , 'students' => $students]);
	}


	public function postBulkAddStudents( Request $request ) {


		try {


			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/students/",$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/students/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/students/".$inputFileName);

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);


			$count=1;


			// add results to data base from file
			foreach($rowData as $cell){


				$password = Str::random(6);

				$student = new student();
				$student->studentid = $cell[0];
				$student->surname = $cell[1];
				$student->othernames = $cell[2];
				$student->society = $cell[3];
				$student->email = $cell[4];
				$student->gender = $cell[5];
				$student->nationality = $cell[6];
				$student->level = $cell[7];
				$student->session = $cell[8];
				$student->prog = $cell[9];
				$student->phone = $cell[10];

				$student->password = $password;
				$status = $student->save();

				mail($cell[4],"Your clearance portal credentials", "You have successfully been added as a potential
		graduand. Please visit the clearance portal at ". url('/') . ". Login with your ID number and this password: $password" );

				if(!empty($cell[10]))
				$this->sendSms($cell[10],"You have successfully been added as a potential
		graduand. Please visit the clearance portal at ". url('/') . ". Login with your ID number and this password: $password");

			}


		}
		catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}

		if($status) {
			$request->session()->flash("success", "Student added successfully!");
			return redirect('/staff/upload-students');
		}
		else{
			$request->session()->flash("error", "Sorry an error occured");
			return redirect('/staff/upload-students');

		}

	}

	function sendSms($phone,$Message){

		/* Variables with the values to be sent. */
		$owneremail="tobennaa@gmail.com";
		$subacct="clearance";
		$subacctpwd="clearance";
		$sendto= $phone; /* destination number */
		$sender="Regent Uni"; /* sender id */

		$message= $Message;  /* message to be sent */

		/* create the required URL */
		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
		       . "&subacct=" . UrlEncode($subacct)
		       . "&subacctpwd=" . UrlEncode($subacctpwd)
		       . "&message=" . UrlEncode($message)
		       . "&sender=" . UrlEncode($sender)
		       ."&sendto=" . UrlEncode($sendto)
		       ."&msgtype=0";


		/* call the URL */
		if ($f = @fopen($url, "r"))  {

			$answer = fgets($f, 255);

			if (substr($answer, 0, 1) == "+") {
//				 "SMS to $dnr was successful.";
			}
//			else  {
//				 "an error has occurred: [$answer].";  }
		}

		else  {   "Error: URL could not be opened.";  }
	}

	public function viewReports() {
		$allStudents = student::all();

		$cleared = student::where('isDeanCleared',1)->get();

		return view('viewReports',[
			'allStudents' => $allStudents,
			'cleared' => $cleared,
			'uncleared' => $allStudents->diff($cleared)
		]);
	}

	public function viewClearedReport(  ) {
		$student = student::where('isDeanCleared',1)->get();

		return view('viewClearedReport',[
			'students' => $student
		]);
	}


	public function viewUnclearedReport(  ) {
		$student = student::where('isDeanCleared',0)->get();

		return view('viewUnclearedReport',[
			'students' => $student
		]);
	}

	public function downloadClearedPDF() {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->viewClearedReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}
	}

	public function downloadUnclearedPDF() {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->viewUnclearedReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}
	}

}
