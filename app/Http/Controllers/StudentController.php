<?php

namespace App\Http\Controllers;

use App\academic;
use App\accounts;
use App\admission;
use App\computerlab;
use App\dean;
use App\department;
use App\electroniclab;
use App\examunit;
use App\finance;
use App\hod;
use App\library;
use App\registry;
use App\society;
use App\student;
use App\studentaffairs;
use App\ess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class StudentController extends Controller
{
	public function __construct(){

		if( !Session::has('student')){
			return  redirect('/students/login');
		}

	}

	public function index(){


		if( !Session::has('student')){
			return redirect('/student/login');
		}


		$accounts = accounts::where('studentid',Session::get('student')->studentid)->first();
		$hod = hod::where('studentid',Session::get('student')->studentid)->first();
		$dean = dean::where('studentid',Session::get('student')->studentid)->first();
		$academic = academic::where('studentid',Session::get('student')->studentid)->first();
		$library = library::where('studentid',Session::get('student')->studentid)->first();
		$ess = ess::where('studentid',Session::get('student')->studentid)->first();
		$society = society::where('studentid',Session::get('student')->studentid)->first();
		$computerlab = computerlab::where('studentid',Session::get('student')->studentid)->first();
		$electroniclab = electroniclab::where('studentid',Session::get('student')->studentid)->first();
		$admission = admission::where('studentid',Session::get('student')->studentid)->first();


		return view('home',
			[
				'accounts' => $accounts,
				'hod' => $hod,
				'academic' => $academic,
				'dean' => $dean,
				'library' => $library,
				'ess' => $ess,
				'society' => $society,
				'computerlab' => $computerlab,
				'electroniclab' => $electroniclab,
				'admission' => $admission
			]);
	}

	public function getCleared($id){

		$response = array();
		$academicStatus = 0;
		$essStatus = 0;
		$libraryStatus= 0;
		$accountsStatus = 0;
		$hodStatus = 0;
		$deanStatus = 0;
		$electroniclabStatus = 0;
		$computerlabStatus = 0;
		$admissionStatus = 0;
		$societyStatus = 0;



		$accounts = accounts::where('studentid',$id)->get()->count();
		$hod = hod::where('studentid',$id)->get()->count();
		$dean = dean::where('studentid',$id)->get()->count();
		$academic = academic::where('studentid',$id)->get()->count();
		$library = library::where('studentid',$id)->get()->count();
		$ess = ess::where('studentid',$id)->get()->count();
		$society = society::where('studentid',$id)->get()->count();
		$computerlab = computerlab::where('studentid',$id)->get()->count();
		$electroniclab = electroniclab::where('studentid',$id)->get()->count();
		$admission = admission::where('studentid',$id)->get()->count();



		if($accounts > 0) $accountsStatus = 1;

		if($hod > 0) $hodStatus = 1;

		if($academic > 0) $academicStatus = 1;

		if($dean > 0) $deanStatus = 1;

		if($society > 0) $societyStatus = 1;

		if($ess > 0) $essStatus = 1;

		if($computerlab > 0) $computerlabStatus = 1;

		if($library > 0) $libraryStatus = 1;

		if($electroniclab > 0) $electroniclabStatus = 1;

		if($admission > 0) $admissionStatus = 1;

		$response = [
			'academic' => $academicStatus,
			'hod' => $hodStatus,
			'accounts' => $accountsStatus,
			'ess' => $essStatus,
			'dean' => $deanStatus,
			'library' => $libraryStatus,
			'society' => $societyStatus,
			'computerlab' => $computerlabStatus,
			'electroniclab' => $electroniclabStatus,
			'admission' => $admissionStatus
		];

		echo json_encode($response);
	}

	public function initiate( Request $request ) {
		if($request->input('inPerson') == "Yes") $answer = 1;
		else $answer = 0;
		$fileUrl = "";
		if($request->hasFile('transcript')){
			$fileName = $request->file('transcript')->getClientOriginalName();
			$request->file('transcript')->move('uploads',$fileName);
			$fileUrl = url('uploads/' . $fileName);
		}
		$student = student::find($request->input('sid'));
		$student->phone = $request->input('phone');
		$student->lastExam = $request->input('lastExam');
		$student->inPerson = $answer;
		$student->recieptNo = $request->input('recieptNo');
		$student->creditPoints = $request->input('creditPoints');
		$student->transcript = $fileUrl;
		$status = $student->save();

		if($status) {
			$request->session()->put('student', $student);
			$request->session()->flash("success", "Clearance process initiated.");
			return redirect('/home');
		} else {
			$request->session()->flash("error", "Sorry something went wrong, please try again.");
			return redirect('/home');
		}
	}
}
