<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

	public function getLogin(){
		if(!Auth::guest()) return redirect('/staff');
		Session::forget('student');
		return view('studentLogin');
	}

	public function postLogin(Request $request){
		$student = student::where('studentid',$request->input('studentid'))->where('password',$request->input('password'))->first();

		if(count($student) > 0 ){
			$request->session()->put('student', $student);
			return redirect('/home');
		} else {
			return view('studentLogin',['status' => 'Invalid Credentials']);
		}

	}



}
