@extends('layouts.app')

@section('content')
<div class="container">
    <div class="logoStuff">
        <img src="{{url('/images/logo.png')}}" class="logo">
        <h3 class="logoHeader">Regent Online Clearance System</h3>
        @if(Session::has("success") )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif

    </div>


    @if( Session::has('student'))
        @if(!isset( Session::get('student')->phone) ||
         !isset( Session::get('student')->lastExam) ||
         !isset( Session::get('student')->inPerson) ||
         !isset( Session::get('student')->recieptNo) ||
         !isset( Session::get('student')->creditPoints)
          )
        
            <div class="col-md-12 intiate">


                <form class="form-group" method="post" enctype="multipart/form-data" action="{{url('/initiate')}}">
                             {{csrf_field()}}

                    <input type="hidden" value="{{Session::get('student')->sid}}" name="sid">

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Name:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="    {{ Session::get('student')->surname}} {{ Session::get('student')->othernames}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Society:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->society}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Email:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->email}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Gender:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->gender}}" disabled >
                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Nationality:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->nationality}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Session:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->session}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Programme:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->prog}}" disabled >
                        </div>
                    </div><br><br>



                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Mobile Number:</label>
                                 
                                 <div class="col-md-6">
                                     <input id="" type="text" pattern="[0-9]+" class="form-control" name="phone" required value="{{ old('phone') }}" >
                                 </div>
                             </div><br><br>
                             
                             
                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Date Of Last Exam:</label>

                                 <div class="col-md-6">
                                     <input id="" type="text" required class="form-control" pattern="[0-9](1,2}/[0-9](1,2}/[0-1][0-9]" placeholder="Eg. 22/04/17" name="lastExam" value="{{ old('lastExam') }}" >
                                 </div>
                             </div><br><br>
                             
                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Graduating in person?</label>
                                 
                                 <div class="col-md-6">
                                     <select name="inPerson"  class="form-control" required >
                                         <option disabled>Please select an option</option>
                                         <option>Yes</option>
                                         <option>No</option>
                                     </select>
                                 </div>
                             </div><br><br>


                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Graduation fee recipt number:</label>

                                 <div class="col-md-6">
                                     <input id="" required type="text" pattern="[0-9]+" class="form-control" name="recieptNo" value="{{ old('recieptNo') }}" >
                                 </div>
                             </div><br><br>


                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Total credit points attained:</label>

                                 <div class="col-md-6">
                                     <input id="" type="text" required class="form-control" name="creditPoints" value="{{ old('creditPoints') }}" >
                                 </div>
                             </div><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Transcript:</label>

                        <div class="col-md-6">
                            <input id="" type="file" required class="form-control" name="transcript" value="{{ old('transcript') }}" >
                        </div>
                    </div><br>

                    <button type="submit" class="btn btn-primary">
                        Initiate
                    </button>
                    <button type="reset" class="btn btn-warning">Clear</button>
                             
                </form>
            </div>
        @else

        @if( Session::has('student'))

            @if(isset( Session::get('student')->isDeanCleared) == 1 )


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Name:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="  {{ Session::get('student')->surname}} {{ Session::get('student')->othernames}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Society:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->society}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Email:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->email}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Gender:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->gender}}" disabled >
                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Nationality:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->nationality}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Session:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->session}}" disabled >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Programme:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ Session::get('student')->prog}}" disabled >
                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Mobile Number:</label>

                        <div class="col-md-6">
                            <input id="" type="text" required class="form-control" disabled value="{{ Session::get('student')->phone }}" >
                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Date Of Last Exam:</label>

                        <div class="col-md-6">
                            <input id="" type="text" required class="form-control" disabled value="{{ Session::get('student')->lastExam }}" >
                        </div>
                    </div><br><br>

                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Graduating in person?</label>

                        <div class="col-md-6">
                            <input id="" type="text" required class="form-control" disabled value="{{ Session::get('student')->inPerson }}" >                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Graduation fee recipt number:</label>

                        <div class="col-md-6">
                            <input id="" type="text" required class="form-control" disabled value="{{ Session::get('student')->recieptNo }}" >
                        </div>
                    </div><br><br>


                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Total credit points attained:</label>

                        <div class="col-md-6">
                            <input id="" type="text" required class="form-control" disabled value="{{ Session::get('student')->creditPoints }}" >
                        </div>
                    </div><br><br><br>


                @endif

         @endif

         <div class="row">

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="library" >
         LIBRARY

         <span></span>
         <p>
             @if(isset($library))
             {{$library->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3  well uncleared " id="society" >
         SOCIETY
         <span></span>
         <p>
             @if(isset($society))
             {{$society->created_at}}
                 @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3  well uncleared " id="computerlab" >
         COMPUTER LAB
         <span></span>
         <p>
             @if(isset($computerlab))
             {{$computerlab->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="electroniclab" >
         ELECTRONIC LAB
         <span></span>
         <p>
             @if(isset($electroniclab))
             {{$electroniclab->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="accounts">
         ACCOUNTS
         <span></span>
         <p>
             @if(isset($accounts))
             {{$accounts->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="academic">
         ACADEMIC
         <span></span>
         <p>
             @if(isset($academic))
             {{$academic->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="admission">
         ADMISSION
         <span></span>
         <p>
             @if(isset($admission))
             {{$admission->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3 well uncleared " id="ess">
         ESS
         <span></span>
         <p>
             @if(isset($ess))
             {{$ess->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>


     <div class="col-md-3 col-sm-3 col-xs-6 well uncleared " id="hod">
         HOD
         <span></span>
         <p>
             @if(isset($hod))
             {{$hod->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>


     <div class="col-md-3 col-sm-3 col-xs-6 col-md-offset-6 well uncleared " id="dean">
         DEAN
         <span></span>
         <p>
             @if(isset($dean))
             {{$dean->created_at}}
             @else
                 &nbsp;
             @endif
         </p>
     </div>



                 <div class="col-md-4 col-md-offset-4" style="margin-top: -150px;">
                     <a id="print" onclick="window.print()" style="margin:10px 45%;"  class="hidden-xs btn btn-primary">Print Slip</a>


                     <p id="info"  align="center">Once you are cleared fully. A print button would appear and you can print your slip.</p>

                 </div>


 </div>

            @if( Session::has('student'))

                @if(isset( Session::get('student')->isDeanCleared) == 1 )

                    {{--<div class="row">--}}
                        {{--<div class="col-md-4 col-xs-4 col-xs-offset-8 col-md-offset-8">--}}
                            {{--<h3 align="center">REGISTRAR</h3> <br><br><br><br><br><br>--}}
                            {{--<hr style="border-bottom: 3px;border-color: black;border-style: dotted;">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                @endif
            @endif

     @endif
 @endif
</div>

 <script>
     $(document).ready(function(){
         var studentid = $('#studentid').text().trim();


         var society = $('#society');
         var hod = $('#hod');
         var computerlab = $('#computerlab');
         var electroniclab = $('#electroniclab');
         var accounts = $('#accounts');
         var admission = $('#admission');
         var dean = $('#dean');
         var academic = $('#academic');
         var library = $('#library');
         var ess = $('#ess');

         var url = '{{url('/cleared')}}/' + studentid;

         getUpdates();

         console.log(studentid);
         setInterval(getUpdates,4000);

     function getUpdates() {
         $.ajax({
             url: url,
             method: 'get',
             success: function (response) {
                 console.log(response);
                 var clearedCount = 0;
                 var status = JSON.parse(response);


                 console.log(url);


                 if (status.computerlab == 1) {
                     computerlab.removeClass('uncleared');
                     computerlab.addClass('cleared');
                     clearedCount++;
                 } else {
                     computerlab.removeClass('cleared');
                     computerlab.addClass('uncleared');
                 }

                 if (status.electroniclab == 1) {
                     electroniclab.removeClass('uncleared');
                     electroniclab.addClass('cleared');
                     clearedCount++;
                 } else {
                     electroniclab.removeClass('cleared');
                     electroniclab.addClass('uncleared');
                 }


                 if (status.dean == 1) {
                     dean.removeClass('uncleared');
                     dean.addClass('cleared');
                     clearedCount++;
                 } else {
                     dean.removeClass('cleared');
                     dean.addClass('uncleared');
                 }


                 if (status.hod == 1) {
                     hod.removeClass('uncleared');
                     hod.addClass('cleared');
                     clearedCount++;
                 } else {
                     hod.removeClass('cleared');
                     hod.addClass('uncleared');
                 }

                 if (status.accounts == 1) {
                     accounts.removeClass('uncleared');
                     accounts.addClass('cleared');
                     clearedCount++;
                 } else {
                     accounts.removeClass('cleared');
                     accounts.addClass('uncleared');
                 }


                 if (status.academic == 1) {
                     academic.removeClass('uncleared');
                     academic.addClass('cleared');
                     clearedCount++;
                 } else {
                     academic.removeClass('cleared');
                     academic.addClass('uncleared');
                 }

                 if (status.admission == 1) {
                     admission.removeClass('uncleared');
                     admission.addClass('cleared');
                     clearedCount++;
                 } else {
                     admission.removeClass('cleared');
                     admission.addClass('uncleared');
                 }


                 if (status.library == 1) {
                     library.removeClass('uncleared');
                     library.addClass('cleared');
                     clearedCount++;
                 } else {
                     library.removeClass('cleared');
                     library.addClass('uncleared');
                 }


                 if (status.ess == 1) {
                     ess.removeClass('uncleared');
                     ess.addClass('cleared');
                     clearedCount++;
                 } else {
                     ess.removeClass('cleared');
                     ess.addClass('uncleared');
                 }


                 if (status.society == 1) {
                     society.removeClass('uncleared');
                     society.addClass('cleared');
                     clearedCount++;
                 } else {
                     society.removeClass('cleared');
                     society.addClass('uncleared');
                 }

                 if (clearedCount >= 10) {

                     $('#print').removeClass("hidden");
                     $('#info').addClass("hidden");
                 }


                 $('.cleared span').html('<i class="fa fa-check-circle" aria-hidden="true"></i> <br>CLEARED');
                 $('.uncleared span').html('<i class="fa fa-times-circle" aria-hidden="true"></i> <br>UNCLEARED');


             },
             error: function (response) {

             }
         })
     }
         $('.cleared span').html('<i class="fa fa-check-circle" aria-hidden="true"></i> <br>CLEARED');
         $('.uncleared span').html('<i class="fa fa-times-circle" aria-hidden="true"></i> <br>UNCLEARED');

     });
 </script>
@endsection
