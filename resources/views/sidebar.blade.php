<div id="sidebar" class="col-md-2 sidebar">
    <ul>
        <li>
            @if(Auth::user()->role == "EXAM UNIT")
                <a href="{{url('/staff/upload-students')}}">UPLOAD STUDENTS</a>
            @else
                @if(Auth::user()->role != "ADMIN")
                <a href="{{url('/staff/clear-students')}}">CLEAR STUDENTS</a>
                @else
                <a href="{{url('/register')}}">REGISTER STAFF</a>
                @endif
            @endif
        </li>
        @if(Auth::user()->role != "EXAM UNIT")
        <li>
            <a href="{{url('/')}}">
                VIEW UNCLEARED STUDENTS
            </a>
        </li>

        <li>
            <a href="{{url('/staff/view-cleared')}}">
                VIEW CLEARED STUDENTS
            </a>
        </li>

        @endif

        <li>
            <a href="{{url('/staff/view-transcript')}}">VIEW TRANSCRIPT</a>
        </li>
        <li>
            <a href="{{url('/staff/view-reports')}}">VIEW REPORTS</a>
        </li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
               LOGOUT
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>

    @if (!Auth::guest())
        <p>
            Logged in as {{Auth::user()->name}}
        </p>
    @endif

</div>
